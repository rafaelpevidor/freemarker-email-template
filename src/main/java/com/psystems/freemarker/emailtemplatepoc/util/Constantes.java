/**
 *
 */
package com.psystems.freemarker.emailtemplatepoc.util;

/**
 * @author rafaelpevidor
 *
 */
public final class Constantes {

    private Constantes() {}

    public static final String CAMINHO_PARA_SALVAR_ARQUIVO = "templateDir";
    public static final String NOME_CLIENTE = "Adolfo Hansen";
    public static final String MENSAGEM_TEMPLATE = "Gingerbread cake croissant pudding biscuit fruitcake tootsie roll jujubes carrot cake. "
            + "Oat cake jelly-o candy canes gummies. Soufflé candy jelly-o lollipop jujubes bonbon chupa chups dessert. Tootsie roll cheesecake sweet. "
            + "Lollipop chocolate cookie chocolate bar. Jelly candy pastry pudding dessert macaroon chocolate bar jelly-o. Carrot cake apple pie macaroon.";
    public static final String EMAIL_CONTATO_TEMPLATE = "st@rlord.com";
    public static final Integer OPCAO_ESCREVER_ARQUIVO = 2;
    public static final Integer OPCAO_ESCREVER_TERMINAL = 1;
    public static final Integer SIM = 2;
    public static final String FALHA_AO_RECUPERAR_ARQUIVO_DE_PROPRIEDADES = "Falha ao recuperar arquivo de propriedades";
    public static final String CAMINHO_ARQUIVO_CONFIGURACOES_SERVIDOR_EMAIL = "mailServerConfigFile";


}
