/**
 *
 */
package com.psystems.freemarker.emailtemplatepoc.dao;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.psystems.freemarker.emailtemplatepoc.model.Produto;

/**
 * @author rafaelpevidor
 *
 */
public class ProdutoDAO {



    public ProdutoDAO() {
        inicializaTabelaDeProdutos();
    }

    private Map<Long, Produto> tabelaDeProdutos = new HashMap<>();

    public Collection<Produto> obterTodos() {
        return Collections.unmodifiableCollection(tabelaDeProdutos.values());
    }

    public void adicionar(final Produto produto) {
        Long produtoId = null != produto.getId()
                ?produto.getId()
                        :new Long(tabelaDeProdutos.size() + 1);
        tabelaDeProdutos.put(produtoId, produto);
    }

    public void remover(final Long id) {
        tabelaDeProdutos.remove(id);
    }

    public Produto obterPor(final Long id) {
        return tabelaDeProdutos.get(id);
    }

    private void inicializaTabelaDeProdutos() {
        tabelaDeProdutos.put(1l, new Produto(1l, "Caneta", "Caneta Esferográfica Azul", new BigDecimal("2.90")));
        tabelaDeProdutos.put(2l, new Produto(2l, "Apontador", "Apontador com depósito Rosa", new BigDecimal("0.90")));
        tabelaDeProdutos.put(3l, new Produto(3l, "Caderno", "Caderno 200 folhas capa dura", new BigDecimal("4.90")));
        tabelaDeProdutos.put(4l, new Produto(4l, "Mochila", "Mochila com três partições", new BigDecimal("84.90")));
    }
}
