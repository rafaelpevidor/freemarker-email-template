/**
 *
 */
package com.psystems.freemarker.emailtemplatepoc.dao;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import com.psystems.freemarker.emailtemplatepoc.model.Pedido;
import com.psystems.freemarker.emailtemplatepoc.model.PedidoItem;
import com.psystems.freemarker.emailtemplatepoc.model.Produto;

/**
 * @author rafaelpevidor
 *
 */
public class PedidoDAO {

    public PedidoDAO() {
        inicializaTabelaDePedidos();
    }

    private Map<Long, Pedido> tabelaDePedidos = new HashMap<>();

    public void adicionar(Pedido pedido) {
        Objects.requireNonNull(pedido, "Pedido e obrigatorio.");

        boolean pedidoNovo = null == pedido.getId();
        long pedidoId = pedido.getId();

        if (pedidoNovo) {
        	pedido.setDataCadastro(new Date());
        	pedido.setId(obterProximoId());
        }

        pedido.setId(pedidoId);
        tabelaDePedidos.put(pedidoId, pedido);
    }

    public void remover(final Long id) {
        tabelaDePedidos.remove(id);
    }

    public Collection<Pedido> obterTodos() {
        return Collections.unmodifiableCollection(tabelaDePedidos.values());
    }

    public Pedido obterPor(final Long id) {
        return tabelaDePedidos.get(id);
    }

    private void inicializaTabelaDePedidos() {
        ProdutoDAO produtoDAO = new ProdutoDAO();

        for (Produto produto : produtoDAO.obterTodos()) {
            Pedido pedido = new Pedido();
            pedido.setId(produto.getId());
            pedido.setCliente("Cliente nro " + produto.getId());
            pedido.setDataCadastro(new Date());
            pedido.setCodigo(pedido.getDataCadastro().getTime() + "");
            pedido.getItens().add(new PedidoItem(produto, new BigDecimal(produto.getId())));

            tabelaDePedidos.put(pedido.getId(), pedido);
        }
    }

    private long obterProximoId() {
		return 1L + new Long(tabelaDePedidos.size()+"");
	}
}
