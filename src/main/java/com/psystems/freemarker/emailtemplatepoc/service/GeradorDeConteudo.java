/**
 *
 */
package com.psystems.freemarker.emailtemplatepoc.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.logging.Logger;

import com.psystems.freemarker.emailtemplatepoc.config.FreeMarkerConfig;
import com.psystems.freemarker.emailtemplatepoc.model.vo.InformacaoTemplateVO;

import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * @author rafaelpevidor
 *
 */
public class GeradorDeConteudo {

    private static final String OCORREU_UM_ERRO_INESPERADO = "Ocorreu um erro inesperado.";
	private static final String FALHA_AO_FECHAR_RECURSOS_DE_ESCRITA_DE_ARQUIVO = "Falha ao fechar recursos de escrita de arquivo.";
    private static final String FALHA_AO_OBTER_E_OU_ESCREVER_O_TEMPLATE = "Falha ao obter e/ou escrever o template.";
    private static final Logger LOGGER = Logger.getLogger(GeradorDeConteudo.class.getName());

    public void escreverParaArquivo(final String nomeTemplate, final InformacaoTemplateVO conteudo, final File destinoArquivo) {
        Writer arquivoSaida = null;
        try {
            Template template = obterTemplate(nomeTemplate);
            String nomeArquivo = destinoArquivo.getPath().concat("/").concat(nomeTemplate).concat(".txt");
            arquivoSaida = new FileWriter(new File(nomeArquivo));
            template.process(conteudo, arquivoSaida);
        } catch (IOException | TemplateException e) {
            throw new RuntimeException(FALHA_AO_OBTER_E_OU_ESCREVER_O_TEMPLATE, e);
        } catch (Exception e) {
            throw new RuntimeException(OCORREU_UM_ERRO_INESPERADO, e);
        } finally {
            try {
                if (null != arquivoSaida) {
                    arquivoSaida.flush();
                    arquivoSaida.close();
                }
            } catch (IOException e) {
                LOGGER.warning(FALHA_AO_FECHAR_RECURSOS_DE_ESCRITA_DE_ARQUIVO);
            }
        }
    }

    public String obterConteudo(final String nomeTemplate, final InformacaoTemplateVO conteudo) {
        Writer out = null;
        try {
            Template template = obterTemplate(nomeTemplate);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            out = new OutputStreamWriter(stream);
            template.process(conteudo, out);
            return new String(stream.toByteArray());
        } catch (IOException | TemplateException e) {
            throw new RuntimeException(FALHA_AO_OBTER_E_OU_ESCREVER_O_TEMPLATE, e);
        } catch (Exception e) {
            throw new RuntimeException(OCORREU_UM_ERRO_INESPERADO, e);
        } finally {
            try {
                if (null != out)
                    out.flush();
            } catch (IOException e) {
                LOGGER.warning(FALHA_AO_FECHAR_RECURSOS_DE_ESCRITA_DE_ARQUIVO);
            }
        }
    }

    public void escrever(final String nomeTemplate, final InformacaoTemplateVO conteudo) {
        System.out.println("\n" + obterConteudo(nomeTemplate, conteudo));
    }

    private Template obterTemplate(final String nomeTemplate)
            throws IOException {
        return FreeMarkerConfig.getInstance().getConfig().getTemplate(nomeTemplate);
    }
}
