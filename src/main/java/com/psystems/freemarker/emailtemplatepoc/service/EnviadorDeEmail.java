package com.psystems.freemarker.emailtemplatepoc.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

import com.psystems.freemarker.emailtemplatepoc.model.vo.EmailVO;
import com.psystems.freemarker.emailtemplatepoc.util.Constantes;

public class EnviadorDeEmail {

    private static final Logger LOGGER = Logger.getLogger(EnviadorDeEmail.class.getName());

    public boolean enviar(final EmailVO email) {
        return enviar(email.getDestinatarios(),
                email.getCc(), email.getBcc(), email.getAssunto(), email.getConteudo());
    }

    protected boolean enviar(final Set<String> emailsDosDestinatarios,
            final Set<String> cc, final Set<String> bcc, final String assunto, final String conteudo) {
        Objects.requireNonNull(emailsDosDestinatarios, "O campo destinatários é obrigatório.");

        HtmlEmail htmlEmail = new HtmlEmail();

        try {
            if (null != bcc) {
                for (String endereco : bcc)
                    htmlEmail.addBcc(endereco);
            }

            if (null != cc) {
                for (String endereco : cc)
                    htmlEmail.addCc(endereco);
            }

            for (String endereco : emailsDosDestinatarios)
                htmlEmail.addTo(endereco);

            Properties mailProperties = obterPropriedadesDoServidorDeEmail();

            htmlEmail.setSubject(assunto);
            htmlEmail.setFrom(mailProperties.getProperty("mail.sender.address"), mailProperties.getProperty("mail.sender.name"));
            htmlEmail.setHostName(mailProperties.getProperty("mail.smtp.host"));
            htmlEmail.setSmtpPort(Integer.parseInt(mailProperties.getProperty("mail.smtp.port")));
            htmlEmail.setAuthenticator(new DefaultAuthenticator(mailProperties.getProperty("mail.smtp.user"),
                    mailProperties.getProperty("mail.provider.password")));
            htmlEmail.setStartTLSEnabled(Boolean.getBoolean(mailProperties.getProperty("mail.smtp.starttls.enable")));
            htmlEmail.setStartTLSRequired(Boolean.getBoolean(mailProperties.getProperty("mail.smtp.starttls.required")));
            htmlEmail.setSSLCheckServerIdentity(Boolean.getBoolean(mailProperties.getProperty("mail.smtp.ssl.checkserveridentity")));
            htmlEmail.setHtmlMsg(conteudo);
            htmlEmail.setCharset(mailProperties.getProperty("mail.charset"));
            htmlEmail.send();

            return true;
        } catch (EmailException e) {
            LOGGER.warning(e.getLocalizedMessage());
        } catch (Exception e) {
            LOGGER.severe(e.getLocalizedMessage());
        }

        return false;
    }

    private Properties obterPropriedadesDoServidorDeEmail() {
        InputStream is = null;
        Properties prop = null;
        try {
            prop = new Properties();
            is = new FileInputStream(new File(System.getProperty(Constantes.CAMINHO_ARQUIVO_CONFIGURACOES_SERVIDOR_EMAIL)));
            prop.load(is);
            return prop;
        } catch (IOException e) {
            LOGGER.severe(Constantes.FALHA_AO_RECUPERAR_ARQUIVO_DE_PROPRIEDADES);
            throw new RuntimeException(Constantes.FALHA_AO_RECUPERAR_ARQUIVO_DE_PROPRIEDADES);
        }
    }
}
