/**
 *
 */
package com.psystems.freemarker.emailtemplatepoc.config;

import freemarker.template.Configuration;

/**
 * @author rafaelpevidor
 *
 */
public class FreeMarkerConfig {

    private FreeMarkerConfig() {
    	this.config = new Configuration(Configuration.VERSION_2_3_30);
    	this.config.setDefaultEncoding("UTF-8");
    	this.config.setClassForTemplateLoading(getClass(), "/templates/");
    }

    private static FreeMarkerConfig instance;
    private Configuration config;

    public static FreeMarkerConfig getInstance() {
        if (null == instance)
            return new FreeMarkerConfig();
        return instance;
    }

    public Configuration getConfig() {
        return config;
    }

}
