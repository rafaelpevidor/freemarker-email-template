/**
 *
 */
package com.psystems.freemarker.emailtemplatepoc.model.vo;

import com.psystems.freemarker.emailtemplatepoc.model.Empresa;
import com.psystems.freemarker.emailtemplatepoc.model.Pedido;

/**
 * @author rafaelpevidor
 *
 */
public class InformacaoTemplateVO {

    public InformacaoTemplateVO(String cliente, String mensagem, String emailDeContato, Pedido pedido, Empresa empresa) {
        this.cliente = cliente;
        this.mensagem = mensagem;
        this.emailDeContato = emailDeContato;
        this.pedido = pedido;
        this.empresa = empresa;
    }

    public InformacaoTemplateVO(String cliente, String mensagem, Empresa empresa) {
        this.cliente = cliente;
        this.mensagem = mensagem;
        this.empresa = empresa;
    }

    private String cliente;

    private String mensagem;

    private String emailDeContato;

    private Pedido pedido;

    private Empresa empresa;

    public String getCliente() {
        return cliente;
    }

    public String getMensagem() {
        return mensagem;
    }

    public String getEmailDeContato() {
        return emailDeContato;
    }

    public void setEmailDeContato(String emailDeContato) {
        this.emailDeContato = emailDeContato;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
}
