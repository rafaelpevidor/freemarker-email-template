/**
 *
 */
package com.psystems.freemarker.emailtemplatepoc.model;

import java.math.BigDecimal;

/**
 * @author rafaelpevidor
 *
 */
public class PedidoItem {

    public PedidoItem(Produto produto, BigDecimal quantidade) {
        this.produto = produto;
        this.quantidade = quantidade;
    }

    public PedidoItem() {}

    private Produto produto;

    private BigDecimal quantidade;

    public BigDecimal getSubtotal() {
        if (null != quantidade && null != produto && null != produto.getValor())
            return quantidade.multiply(produto.getValor());
        else
            return BigDecimal.ZERO;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((produto == null) ? 0 : produto.hashCode());
        result = prime * result + ((quantidade == null) ? 0 : quantidade.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PedidoItem other = (PedidoItem) obj;
        if (produto == null) {
            if (other.produto != null)
                return false;
        } else if (!produto.equals(other.produto))
            return false;
        if (quantidade == null) {
            if (other.quantidade != null)
                return false;
        } else if (!quantidade.equals(other.quantidade))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "PedidoItem [produto=" + produto + ", quantidade=" + quantidade + "]";
    }
}
