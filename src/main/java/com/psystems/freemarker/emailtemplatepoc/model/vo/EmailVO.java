/**
 *
 */
package com.psystems.freemarker.emailtemplatepoc.model.vo;

import java.util.Set;

/**
 * @author rafaelpevidor
 *
 */
public class EmailVO {

    public EmailVO(String assunto, String conteudo, Set<String> destinatarios) {
        this.assunto = assunto;
        this.conteudo = conteudo;
        this.destinatarios = destinatarios;
    }

    private String emailRemetente;

    private String nomeRemetente;

    private String assunto;

    private String conteudo;

    private Set<String> destinatarios;

    private Set<String> cc;

    private Set<String> bcc;

    public String getEmailRemetente() {
        return emailRemetente;
    }

    public void setEmailRemetente(String emailRemetente) {
        this.emailRemetente = emailRemetente;
    }

    public String getNomeRemetente() {
        return nomeRemetente;
    }

    public void setNomeRemetente(String nomeRemetente) {
        this.nomeRemetente = nomeRemetente;
    }

    public String getAssunto() {
        return assunto;
    }

    public String getConteudo() {
        return conteudo;
    }

    public Set<String> getDestinatarios() {
        return destinatarios;
    }

    public Set<String> getCc() {
        return cc;
    }

    public void setCc(Set<String> cc) {
        this.cc = cc;
    }

    public Set<String> getBcc() {
        return bcc;
    }

    public void setBcc(Set<String> bcc) {
        this.bcc = bcc;
    }
}
