/**
 *
 */
package com.psystems.freemarker.emailtemplatepoc.model;

/**
 * @author rafaelpevidor
 *
 */
public class Empresa {

    private String nomeEmpresa = "InfoPaper";

    private String endereco = "Rua das Aroeiras";

    private String numero = "190";

    private String complemento = "Loja 8";

    private String bairro = "Floresta Negra";

    private String cidade = "Lemanha";

    private String uf = "AJ";

    private String cep = "30987654";

    private String ddd = "28";

    private String telefone = "22345890";

    private String cnpj = "00000000000000";

    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    public void setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getDdd() {
        return ddd;
    }

    public void setDdd(String ddd) {
        this.ddd = ddd;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

	@Override
	public String toString() {
		return "Empresa [nomeEmpresa=" + nomeEmpresa + ", endereco=" + endereco + ", numero=" + numero
				+ ", complemento=" + complemento + ", bairro=" + bairro + ", cidade=" + cidade + ", uf=" + uf + ", cep="
				+ cep + ", ddd=" + ddd + ", telefone=" + telefone + ", cnpj=" + cnpj + "]";
	}
}
