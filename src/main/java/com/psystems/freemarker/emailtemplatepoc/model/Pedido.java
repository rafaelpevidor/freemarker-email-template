/**
 *
 */
package com.psystems.freemarker.emailtemplatepoc.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author rafaelpevidor
 *
 */
public class Pedido {

    private Long id;

    private String codigo;

    private String cliente;

    private Date dataCadastro;

    private String enderecoEntrega;

    private BigDecimal subtotal = BigDecimal.ZERO;

    private BigDecimal encargos = BigDecimal.ZERO;

    private BigDecimal frete = BigDecimal.ZERO;

    private BigDecimal desconto = BigDecimal.ZERO;

	private BigDecimal valorTotal = BigDecimal.ZERO;

    private Set<PedidoItem> itens = new HashSet<>();

    public void add(PedidoItem item) {
        itens.add(item);
        calcularSubTotal();
    }

    public void remove(PedidoItem item) {
        itens.remove(item);
        calcularSubTotal();
    }

    protected BigDecimal calcularSubTotal() {
    	if (null == itens || itens.isEmpty())
    		return BigDecimal.ZERO;
    	else {
    		itens.forEach(pedidoItem ->  subtotal = subtotal.add(pedidoItem.getSubtotal()));
    		return subtotal;
    	}
    }

    protected BigDecimal calcularTotal() {
		return getSubtotal().add(getEncargos()).add(getFrete()).subtract(getDesconto());
	}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public BigDecimal getValorTotal() {
    	setValorTotal(calcularTotal());
        return valorTotal;
    }

    private void setValorTotal(BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }

    public String getEnderecoEntrega() {
		return enderecoEntrega;
	}

	public void setEnderecoEntrega(String enderecoEntrega) {
		this.enderecoEntrega = enderecoEntrega;
	}

    public Set<PedidoItem> getItens() {
        return itens;
    }

    @SuppressWarnings("unused")
    private void setItens(Set<PedidoItem> items) {
        this.itens = items;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public BigDecimal getSubtotal() {
    	setSubtotal(calcularSubTotal());
		return subtotal;
	}

	private void setSubtotal(BigDecimal subTotal) {
		this.subtotal = subTotal;
	}

	public BigDecimal getEncargos() {
		return encargos;
	}

	public void setEncargos(BigDecimal encargos) {
		this.encargos = encargos;
	}

	public BigDecimal getFrete() {
		return frete;
	}

	public void setFrete(BigDecimal frete) {
		this.frete = frete;
	}

	public BigDecimal getDesconto() {
		return desconto;
	}

	public void setDesconto(BigDecimal desconto) {
		this.desconto = desconto;
	}

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Pedido other = (Pedido) obj;
        if (codigo == null) {
            if (other.codigo != null)
                return false;
        } else if (!codigo.equals(other.codigo))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Pedido [id=" + id + ", codigo=" + codigo + ", cliente=" + cliente + ", dataCadastro=" + dataCadastro
                + ", valorTotal=" + valorTotal + ", items=" + itens + "]";
    }

}
