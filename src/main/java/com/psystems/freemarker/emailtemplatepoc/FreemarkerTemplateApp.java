/**
 *
 */
package com.psystems.freemarker.emailtemplatepoc;

import java.io.File;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.psystems.freemarker.emailtemplatepoc.dao.ProdutoDAO;
import com.psystems.freemarker.emailtemplatepoc.model.Empresa;
import com.psystems.freemarker.emailtemplatepoc.model.Pedido;
import com.psystems.freemarker.emailtemplatepoc.model.PedidoItem;
import com.psystems.freemarker.emailtemplatepoc.model.vo.EmailVO;
import com.psystems.freemarker.emailtemplatepoc.model.vo.InformacaoTemplateVO;
import com.psystems.freemarker.emailtemplatepoc.service.EnviadorDeEmail;
import com.psystems.freemarker.emailtemplatepoc.service.GeradorDeConteudo;
import com.psystems.freemarker.emailtemplatepoc.util.Constantes;

/**
 * @author rafaelpevidor
 *
 */
public class FreemarkerTemplateApp {

    private static Map<Integer, String> templates = new HashMap<>();
    private static Empresa empresa = new Empresa();
    private static String destinatario = "";
    private static Pedido pedido = null;

    static {
        templates.put(1, "primeiro-template.ftlh");
        templates.put(2, "segundo-template.ftlh");
        templates.put(3, "template-aniversario.ftlh");
        templates.put(4, "template-assinatura.ftlh");
        templates.put(5, "template-formatura.ftlh");
        templates.put(6, "template-pedido.ftlh");
        templates.put(7, "template-pgto-recebido.ftlh");
    }


    /**
     * @param args
     */
    public static void main(String[] args) {

        try (Scanner scanner = new Scanner(System.in);) {

            Integer opcaoEscolhida = obterInformacaoSobreOTemplate(scanner);
            Integer opcaoEscolhidaParaEnvioDeEmail = obterInformacaoSobreEnvioDeEmail(scanner);
            Integer opcaoEscolhidaParaEscrita = obterInformacaoSobreEscritaDoTemplate(scanner);

            File diretorio = null;
            GeradorDeConteudo gerador = new GeradorDeConteudo();

            if (Constantes.OPCAO_ESCREVER_ARQUIVO == opcaoEscolhidaParaEscrita) {
                String destinoArquivo = "";

                System.out.print("Informe o local onde o arquivo sera salvo: ");

                destinoArquivo = scanner.next();

                diretorio = new File(destinoArquivo);

                if (!diretorio.exists() || !diretorio.isDirectory()) {
                    System.out.println("Destino informado invalido. A aplicacao ira gerar o arquivo no local padrao.");
                    diretorio = new File(System.getProperty(Constantes.CAMINHO_PARA_SALVAR_ARQUIVO));
                }

                escreverEmArquivo(opcaoEscolhida, diretorio, gerador);
            } else {
                escreverNoTerminal(opcaoEscolhida, gerador);
            }

            if (opcaoEscolhidaParaEnvioDeEmail.equals(Constantes.SIM)) {
                String conteudoEmail = gerador.obterConteudo(templates.get(opcaoEscolhida),
                      new InformacaoTemplateVO(pedido.getCliente(),
                      Constantes.MENSAGEM_TEMPLATE, Constantes.EMAIL_CONTATO_TEMPLATE, pedido, empresa));
                Set<String> destinatarios = new HashSet<>();
                destinatarios.add(destinatario);
                new EnviadorDeEmail().enviar(new EmailVO("Este e-mail eh um template... " + Calendar.getInstance().getTimeInMillis(),
                        conteudoEmail, destinatarios));
            }

            imprimirMensagemDeFim();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            imprimirMensagemDeFim();
        }
    }

    private static Integer obterInformacaoSobreEnvioDeEmail(Scanner scanner) {
        System.out.println("Deseja enviar email de exemplo?");
        System.out.println("1-Nao, 2-Sim");
        System.out.print("Opção: ");

        Integer opcaoEscolhidaParaEnvioDeEmail = null;
        try {
            opcaoEscolhidaParaEnvioDeEmail = Integer.parseInt(scanner.next());
            if (opcaoEscolhidaParaEnvioDeEmail < Constantes.OPCAO_ESCREVER_TERMINAL ||
                    opcaoEscolhidaParaEnvioDeEmail > Constantes.OPCAO_ESCREVER_ARQUIVO)
                throw new NumberFormatException();
        } catch (NumberFormatException e) {
            imprimirMensagemDeOpcaoInvalida();
            encerrarPrograma();
        }

        System.out.println("Digite o e-mail do destinatario:");
        System.out.print(": ");
        destinatario = scanner.next();

        validarEmailDoDestinatario(destinatario);

        return opcaoEscolhidaParaEnvioDeEmail;
    }

    private static void validarEmailDoDestinatario(String emailDoDestinatario) {
        Pattern pattern = Pattern.compile("^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$");
        Matcher matcher = pattern.matcher(emailDoDestinatario);

        if (!matcher.matches())
            throw new IllegalArgumentException("E-mail invalido.");
    }

    private static Integer obterInformacaoSobreOTemplate(Scanner scanner) {
        System.out.println("Informe o tipo de template a ser gerado:");
        System.out.println("0-Encerrar.");
        System.out.println("1-Texto");
        System.out.println("2-Html simples");
        System.out.println("3-Html aniversario");
        System.out.println("4-Html assinatura");
        System.out.println("5-Html formatura");
        System.out.println("6-Html pedido");
        System.out.println("7-Html pagamento recebido");
        System.out.print("Opção: ");

        Integer opcaoEscolhida = null;
        try {
            opcaoEscolhida = Integer.parseInt(scanner.next());
        } catch (NumberFormatException e) {
            imprimirMensagemDeOpcaoInvalida();
            encerrarPrograma();
        }
        return opcaoEscolhida;
    }

    private static Integer obterInformacaoSobreEscritaDoTemplate(Scanner scanner) {
        System.out.println("Informe o tipo de escrita:");
        System.out.println("1-Terminal, 2-Arquivo");
        System.out.print("Opção: ");

        Integer opcaoEscolhidaParaEscrita = null;
        try {
            opcaoEscolhidaParaEscrita = Integer.parseInt(scanner.next());
            if (opcaoEscolhidaParaEscrita < Constantes.OPCAO_ESCREVER_TERMINAL ||
                    opcaoEscolhidaParaEscrita > Constantes.OPCAO_ESCREVER_ARQUIVO)
                throw new NumberFormatException();
        } catch (NumberFormatException e) {
            imprimirMensagemDeOpcaoInvalida();
            encerrarPrograma();
        }
        return opcaoEscolhidaParaEscrita;
    }

    private static void escreverEmArquivo(final Integer opcaoEscolhida, final File diretorio, final GeradorDeConteudo gerador) {

        InformacaoTemplateVO conteudo;
        String template = validarOpcaoEscolhidaEObterTemplate(opcaoEscolhida);

        conteudo = new InformacaoTemplateVO(pedido.getCliente(), Constantes.MENSAGEM_TEMPLATE, Constantes.EMAIL_CONTATO_TEMPLATE, pedido, empresa);
        new GeradorDeConteudo().escreverParaArquivo(template, conteudo, diretorio);
    }

    private static void escreverNoTerminal(Integer opcaoEscolhida, final GeradorDeConteudo gerador) {

        InformacaoTemplateVO conteudo;
        String template = validarOpcaoEscolhidaEObterTemplate(opcaoEscolhida);

        conteudo = new InformacaoTemplateVO(pedido.getCliente(), Constantes.MENSAGEM_TEMPLATE, Constantes.EMAIL_CONTATO_TEMPLATE, pedido, empresa);
        new GeradorDeConteudo().escrever(template, conteudo);
    }

    private static String validarOpcaoEscolhidaEObterTemplate(Integer opcaoEscolhida) {

        switch (opcaoEscolhida) {
        case 1: case 2: case 3: case 4: case 5: case 6: case 7:
            pedido = getPedido(new ProdutoDAO());
            return templates.get(opcaoEscolhida);
        case 0:
            encerrarPrograma();
            break;
        default:
            imprimirMensagemDeOpcaoInvalida();
            encerrarPrograma();
            break;
        }
        return null;
    }

    private static void encerrarPrograma() {
        imprimirMensagemDeFim();
        System.exit(0);
    }

    private static void imprimirMensagemDeOpcaoInvalida() {
        System.out.println("Opção escolhida é inválida.");
    }

    private static void imprimirMensagemDeFim() {
        System.out.println("FIM.");
    }

    private static Pedido getPedido(ProdutoDAO produtoDAO) {
        Pedido pedidoGrande = new Pedido();
        pedidoGrande.setCliente(Constantes.NOME_CLIENTE);
        pedidoGrande.setCodigo("123ASDF564");
        pedidoGrande.setDataCadastro(new Date());
        pedidoGrande.add(new PedidoItem(produtoDAO.obterPor(1l), new BigDecimal(2)));
        pedidoGrande.add(new PedidoItem(produtoDAO.obterPor(2l), new BigDecimal(3)));
        pedidoGrande.add(new PedidoItem(produtoDAO.obterPor(3l), new BigDecimal(4)));
        pedidoGrande.add(new PedidoItem(produtoDAO.obterPor(4l), new BigDecimal(1)));
        pedidoGrande.setEnderecoEntrega("Rua das Batatas, 110 - Nova Horta");
        return pedidoGrande;
    }

}
